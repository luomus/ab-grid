<?php

namespace AbGrid\Type;


class Bootsrap extends AbstractType
{
    protected $rowClass = 'row';

    protected $gridClasses = [
        'xs' => 'col-xs-%s',
        'sm' => 'col-sm-%s',
        'md' => 'col-md-%s',
        'lg' => 'col-lg-%s',
        'push-xs' => 'col-xs-push-%s',
        'push-sm' => 'col-sm-push-%s',
        'push-md' => 'col-md-push-%s',
        'push-lg' => 'col-lg-push-%s',
        'pull-xs' => 'col-xs-pull-%s',
        'pull-sm' => 'col-sm-pull-%s',
        'pull-md' => 'col-md-pull-%s',
        'pull-lg' => 'col-lg-pull-%s',
        'offset-xs' => 'col-xs-offset-%s',
        'offset-sm' => 'col-sm-offset-%s',
        'offset-md' => 'col-md-offset-%s',
        'offset-lg' => 'col-lg-offset-%s',
    ];

}