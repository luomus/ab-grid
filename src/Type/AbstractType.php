<?php
namespace AbGrid\Type;

abstract class AbstractType implements TypeInterface
{
    protected $gridClasses = [];
    protected $rowClass = '';
    protected $colTag = 'div';
    protected $rowTag = 'div';
    protected $elemWrap = '{{%s}}';
    protected $fieldContainerClass = '';

    /**
     * Converts template to html
     *
     * @param $template
     * @return string
     */
    public function getGrid($template)
    {
        while (preg_match('{(?<=\()[^\(.]+?(?=\))}', $template, $match)) {
            $template = str_replace('('.$match[0].')', $this->getGrid($match[0]), $template);
        }
        $rows = explode("\n", $template);
        foreach ($rows as $key => &$line) {
            $hasField = false;
            while (preg_match('{(?P<grid>\|[\d\-]+([<>_]?[\d\-]+){0,2})\s?(?P<field>\w+)?(?P<rest>[^\|]+)?}', $line, $match)) {
                $hasField = true;
                $line = str_replace(
                    $match[0],
                    $this->getFieldWrap($match),
                    $line
                );
            }
            if ($hasField && !empty($this->rowClass)) {
                $line = $this->htmlTag($this->colTag, [$this->rowClass], $line);
            }
        }
        return implode('', $rows);
    }

    protected function getFieldWrap($instructions) {
        $classes = [];
        if (!empty($instructions['grid'])) {
            $this->getGridClasses($classes, $instructions['grid'], '\|');
            $this->getGridClasses($classes, $instructions['grid'], '>', 'push-');
            $this->getGridClasses($classes, $instructions['grid'], '<', 'pull-');
            $this->getGridClasses($classes, $instructions['grid'], '_', 'offset-');
        }
        $content = '';
        if (!empty($instructions['field'])) {
            $content = sprintf($this->elemWrap, $instructions['field']);
            $classes[] = sprintf($this->fieldContainerClass, $instructions['field']);
        }
        $content .= empty($instructions['rest']) ? '' : $instructions['rest'];

        return $this->htmlTag($this->rowTag, $classes, $content);
    }

    /**
     * Added the classes found from the template to the classes array
     *
     * @param array $classes
     * @param string $instruction
     * @param string $indicator
     * @param string $prefix
     */
    protected function getGridClasses(array &$classes, $instruction, $indicator, $prefix = '') {
        if (preg_match('{'.$indicator.'(?P<xs>\d{1,2})?\-?(?P<sm>\d{1,2})?\-?(?P<md>\d{1,2})?\-?(?P<lg>\d{1,2})?}', $instruction, $match)) {
            foreach(['xs','sm','md','lg'] as $media) {
                $gridKey = $prefix . $media;
                if (!empty($match[$media]) && isset($this->gridClasses[$gridKey])){
                    $classes[] = sprintf($this->gridClasses[$gridKey], $this->getLength($match[$media]));
                }
            }
        }
    }

    /**
     * Returns the column length
     *
     * @param int $len
     * @return mixed
     */
    protected function getLength($len) {
        return $len;
    }

    /**
     * Return the html tag
     *
     * @param string $tag
     * @param array $classes
     * @param string $content
     * @return string
     */
    protected function htmlTag($tag, array $classes = [], $content = '') {
        $elem = '<' . $tag;
        if (!empty($classes)) {
            $elem .= ' class="' . trim(implode(' ', $classes)) . '"';
        }
        $elem .='>' . $content . '</' . $tag . '>';

        return $elem;
    }

    /**
     * @return string
     */
    public function getColTag()
    {
        return $this->colTag;
    }

    /**
     * Sets the tag used for
     *
     * @param string $colTag
     */
    public function setColTag($colTag)
    {
        $this->colTag = $colTag;
    }

    /**
     * @return string
     */
    public function getRowTag()
    {
        return $this->rowTag;
    }

    /**
     * @param string $rowTag
     */
    public function setRowTag($rowTag)
    {
        $this->rowTag = $rowTag;
    }

    /**
     * @return string
     */
    public function getRowClass()
    {
        return $this->rowClass;
    }

    /**
     * @param string $rowClass
     */
    public function setRowClass($rowClass)
    {
        $this->rowClass = $rowClass;
    }

    /**
     * @return string
     */
    public function getElemWrap()
    {
        return $this->elemWrap;
    }

    /**
     * @param string $elemWrap
     */
    public function setElemWrap($elemWrap)
    {
        $this->elemWrap = $elemWrap;
    }

    /**
     * @return string
     */
    public function getFieldContainerClass()
    {
        return $this->fieldContainerClass;
    }

    /**
     * @param string $fieldContainerClass
     */
    public function setFieldContainerClass($fieldContainerClass)
    {
        $this->fieldContainerClass = $fieldContainerClass;
    }
}