<?php

namespace AbGrid\Type;


interface TypeInterface
{
    public function getGrid($template);
}