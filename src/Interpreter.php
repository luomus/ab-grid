<?php

namespace AbGridTest;


use AbGrid\Type\TypeInterface;

class Interpreter
{

    /** @var TypeInterface */
    protected $type;

    public function __construct(TypeInterface $outputType)
    {
        $this->type = $outputType;
    }

    public function getGrid($template) {
        return $this->type->getGrid($template);
    }

    /**
     * @return TypeInterface
     */
    public function getOutputType()
    {
        return $this->type;
    }

    /**
     * @param TypeInterface $type
     */
    public function setOutputType(TypeInterface $type)
    {
        $this->type = $type;
    }
}