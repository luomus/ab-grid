<?php

namespace AbGridTest\Template\Type;


use AbGrid\Type\Bootsrap;

class BootstrapTest extends \PHPUnit_Framework_TestCase
{
    /** @var Bootsrap */
    protected $template;

    public function setUp()
    {
        $this->template = new Bootsrap();
    }

    /**
     * @dataProvider templateInterpretationsProvide
     * @param $template
     * @param $interpretation
     */
    public function testInterpretingTemplate($template, $interpretation)
    {
        $this->assertEquals($interpretation, $this->template->getGrid($template));
    }

    public function templateInterpretationsProvide()
    {
        return [
            ['test', 'test'],
            ['|---4 test', '<div class="row"><div class="col-lg-4">{{test}}</div></div>'],
            ['|---4>---2 test', '<div class="row"><div class="col-lg-4 col-lg-push-2">{{test}}</div></div>'],
            ['|---4<-1 test', '<div class="row"><div class="col-lg-4 col-sm-pull-1">{{test}}</div></div>'],
            ['|---4_-4 test', '<div class="row"><div class="col-lg-4 col-sm-offset-4">{{test}}</div></div>'],
            ['|1-2-3-4 all', '<div class="row"><div class="col-xs-1 col-sm-2 col-md-3 col-lg-4">{{all}}</div></div>'],
            ['|---4 col|6div', '<div class="row"><div class="col-lg-4">{{col}}</div><div class="col-xs-6">{{div}}</div></div>'],
            ['|---4 div(|--3 foo)', '<div class="row"><div class="col-lg-4">{{div}}<div class="row"><div class="col-md-3">{{foo}}</div></div></div></div>'],
            ["|12 test\n|-3 foo", '<div class="row"><div class="col-xs-12">{{test}}</div></div><div class="row"><div class="col-sm-3">{{foo}}</div></div>'],
            ["|--6(|-6 A|-6 B)|--6(|-3 C|-9 D)", '<div class="row"><div class="col-md-6"><div class="row"><div class="col-sm-6">{{A}}</div><div class="col-sm-6">{{B}}</div></div></div><div class="col-md-6"><div class="row"><div class="col-sm-3">{{C}}</div><div class="col-sm-9">{{D}}</div></div></div></div>'],
            ["|--6(|-6A\n|-6B)|--6(|-3C\n|-9D)", '<div class="row"><div class="col-md-6"><div class="row"><div class="col-sm-6">{{A}}</div></div><div class="row"><div class="col-sm-6">{{B}}</div></div></div><div class="col-md-6"><div class="row"><div class="col-sm-3">{{C}}</div></div><div class="row"><div class="col-sm-9">{{D}}</div></div></div></div>'],
        ];
    }

    public function testTemplateWithoutRow() {
        $this->template->setRowClass('');
        $template = "|-3foo\n|3bar";
        $this->assertEquals('<div class="col-sm-3">{{foo}}</div><div class="col-xs-3">{{bar}}</div>',$this->template->getGrid($template));
    }

    public function testCustomContainerClass() {
        $this->template->setFieldContainerClass('container-%s');
        $template = "|-3foo";
        $this->assertEquals('<div class="row"><div class="col-sm-3 container-foo">{{foo}}</div></div>',$this->template->getGrid($template));
    }
}